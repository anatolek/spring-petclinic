FROM anatolek/maven:latest as mvn
WORKDIR /app
COPY . .
RUN mvn package # -Dmaven.test.skip=true

FROM java:alpine
WORKDIR /app
RUN addgroup -g 1000 -S app && \
  adduser -u 1000 -S appuser -G app
USER appuser
COPY --from=mvn --chown=appuser:app /app/target/*.jar ./pc.jar
EXPOSE 8080
CMD ["java","-jar","/app/pc.jar"]